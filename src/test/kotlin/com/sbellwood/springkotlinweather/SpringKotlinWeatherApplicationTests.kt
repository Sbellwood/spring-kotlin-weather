package com.sbellwood.springkotlinweather

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.client.getForEntity
import org.springframework.http.HttpStatus

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class SpringKotlinWeatherApplicationTests(@Autowired val restTemplate: TestRestTemplate) {

    @Test
    fun assertWeatherPage() {
        val entity = restTemplate.getForEntity<String>("/weather")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(entity.body).contains("<h1>5-Day Weather and Messages Forecast</h1>")
    }

    @Test
    fun assertNonMappedPage() {
        val entity = restTemplate.getForEntity<String>("/")
        assertThat(entity.statusCode).isEqualTo(HttpStatus.NOT_FOUND)
    }

}
