package com.sbellwood.springkotlinweather.repositories

import com.sbellwood.springkotlinweather.api.model.response.ForecastResponse
import com.sbellwood.springkotlinweather.api.service.WeatherService
import com.sbellwood.springkotlinweather.enum.MessageType
import com.sbellwood.springkotlinweather.models.Forecast
import com.sbellwood.springkotlinweather.models.WeatherEvent
import org.springframework.stereotype.Service
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

@Service
class OpenWeatherApiRepository {

    var openWeatherService: WeatherService = WeatherService()

    fun fetchFiveDayForecast(): List<Forecast> {

        openWeatherService
                .weatherInterface
                .getFiveDayForecast(
                        locationString = "minneapolis,us",
                        unitType = "imperial",
                        apiKey = "09110e603c1d5c272f94f64305c09436"
                ).execute().let { apiResponse ->
                    if (apiResponse.isSuccessful) {
                        apiResponse.body()?.let { responseBody ->
                            val forecasts = mutableListOf<Forecast>()
                            extractUniqueForecastDays(responseBody, forecasts)
                            populateForecastsWithWeatherData(responseBody, forecasts)
                            return forecasts
                        }
                        return emptyList()
                    } else {
                        return emptyList()
                    }
                }
    }

    private fun populateForecastsWithWeatherData(response: ForecastResponse, forecasts: MutableList<Forecast>) {
        for (weatherResponse in response.list) {

            val conditionsList = weatherResponse.weather.map { it.main }

            // use a private helper method to calculate the communication method
            val messageType = calculateCommunicationMethod(weatherResponse.main.temp, conditionsList)

            // calculate the days between current time and weather data, used as a proxy for the forecast index
            val forecastIndex = ChronoUnit.DAYS.between(LocalDateTime.now().toLocalDate(), weatherResponse.dt.toLocalDate()).toInt()

            // populate the weather event list with the weather data instance
            forecasts[forecastIndex].weatherEvents.add(WeatherEvent(
                    time = weatherResponse.dt,
                    temp = weatherResponse.main.temp.toInt(),
                    conditions = conditionsList.joinToString(),
                    messageType = messageType
            ))
        }
    }

    private fun extractUniqueForecastDays(response: ForecastResponse, forecasts: MutableList<Forecast>) {
        val cityName = response.city.name
        val countryName = response.city.country

        // extracts the unique forecast days
        val forecastByDay = response.list.distinctBy {
            it.dt.dayOfWeek
        }

        // setup a forecast for each unique day, with an empty list of weather events for the hourly weather
        for (forecast in forecastByDay) {
            val newForecastDay = Forecast(
                    date = forecast.dt,
                    weatherEvents = mutableListOf(),
                    cityName = cityName,
                    countryName = countryName
            )
            forecasts.add(newForecastDay)
        }
    }

    private fun calculateCommunicationMethod(temp: Double, conditions: List<String>): MessageType {
        // requirements describe "Rainy" which could be satisfied by various different weather conditions
        val rainyConditions = arrayListOf("Rain", "Drizzle", "Thunderstorm")

        val isRainy = rainyConditions.any {
            it in conditions
        }

        if (temp < 55.0 || isRainy) {
            return MessageType.PHONE_CALL
        } else if (temp > 75.0 && conditions.contains("Clear")) {
            return MessageType.TEXT
        } else if (temp in 55.0..75.0) {
            return MessageType.EMAIL
        }
        // Using Email as default return type for edge cases not covered by requirements i.e. greater than 75F but not sunny
        return MessageType.EMAIL
    }
}