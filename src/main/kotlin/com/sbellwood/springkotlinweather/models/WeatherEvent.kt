package com.sbellwood.springkotlinweather.models

import com.sbellwood.springkotlinweather.enum.MessageType
import java.time.LocalDateTime

data class WeatherEvent(
    val time: LocalDateTime,
    val temp: Int,
    val conditions: String,
    val messageType: MessageType
)
