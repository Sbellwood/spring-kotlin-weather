package com.sbellwood.springkotlinweather.models

import java.time.LocalDateTime

data class Forecast(
    val date: LocalDateTime,
    var weatherEvents: MutableList<WeatherEvent>,
    val cityName: String,
    val countryName: String
)