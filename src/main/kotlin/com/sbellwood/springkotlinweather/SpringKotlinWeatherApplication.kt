package com.sbellwood.springkotlinweather

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringKotlinWeatherApplication

fun main(args: Array<String>) {
    runApplication<SpringKotlinWeatherApplication>(*args)
}
