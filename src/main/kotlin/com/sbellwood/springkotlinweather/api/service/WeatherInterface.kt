package com.sbellwood.springkotlinweather.api.service

import com.sbellwood.springkotlinweather.api.model.response.ForecastResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherInterface {

    // defines a GET HTTP request with the necessary query requirements
    @GET("forecast")
    fun getFiveDayForecast(
           @Query("q") locationString: String,
           @Query("units") unitType: String,
           @Query("APPID") apiKey: String
    ): Call<ForecastResponse>
}