package com.sbellwood.springkotlinweather.api.service

import com.sbellwood.springkotlinweather.api.client.OpenWeatherMapApiClient

// constructor provides a default apiUrl value, for convenience
class WeatherService(
        val apiUrl: String = "http://api.openweathermap.org/data/2.5/"
) {
    var weatherInterface: WeatherInterface

    init {
        OpenWeatherMapApiClient.shared.setup(apiUrl = apiUrl)

        this.weatherInterface = OpenWeatherMapApiClient.weatherInterface
    }
}