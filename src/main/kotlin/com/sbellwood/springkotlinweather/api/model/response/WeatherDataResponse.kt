package com.sbellwood.springkotlinweather.api.model.response

data class WeatherDataResponse(
    val temp: Double
)