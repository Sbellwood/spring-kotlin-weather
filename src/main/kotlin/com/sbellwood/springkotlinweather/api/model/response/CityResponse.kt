package com.sbellwood.springkotlinweather.api.model.response

data class CityResponse(
    val name: String,
    val country: String
)