package com.sbellwood.springkotlinweather.api.model.response

import java.time.LocalDateTime

data class WeatherResponse(
    val dt: LocalDateTime,
    val main: WeatherDataResponse,
    val weather: List<WeatherTypeResponse>
)