package com.sbellwood.springkotlinweather.api.model.response

data class ForecastResponse(
    val list: List<WeatherResponse>,
    val city: CityResponse
)