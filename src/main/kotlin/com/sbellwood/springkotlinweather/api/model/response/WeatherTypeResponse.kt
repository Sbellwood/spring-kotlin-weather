package com.sbellwood.springkotlinweather.api.model.response

data class WeatherTypeResponse(
    val main: String
)