package com.sbellwood.springkotlinweather.api.client

import com.sbellwood.springkotlinweather.api.service.WeatherInterface

object OpenWeatherMapApiClient {
    var apiUrl: String = ""
    val shared = OpenWeatherMapApiClient

    val weatherInterface: WeatherInterface
        get() = RetrofitClient.getClient(apiUrl)!!.create(
                WeatherInterface::class.java
        )

    // this function allows us to inject an API URL into the RetrofitClient creation process
    fun setup(apiUrl: String): OpenWeatherMapApiClient {
        OpenWeatherMapApiClient.apiUrl = apiUrl
        return shared
    }
}