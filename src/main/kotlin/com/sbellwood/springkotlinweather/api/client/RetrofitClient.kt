package com.sbellwood.springkotlinweather.api.client

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.time.Instant
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit

object RetrofitClient {

    var retrofit: Retrofit? = null

    fun getClient(baseUrl: String): Retrofit? {
        // setup the retrofit client if necessary
        if (retrofit == null) {

            val client = OkHttpClient.Builder()
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .build()

            // creates a custom type adapter to bring UNIX timestamp to a LocalDateTime
            val gson = GsonBuilder()
                    .registerTypeAdapter(
                        LocalDateTime::class.java,
                        JsonDeserializer { json, typeOfT, context ->
                            LocalDateTime.ofInstant(Instant.ofEpochSecond(json.asJsonPrimitive.asLong), TimeZone.getDefault().toZoneId())
                        }
                    ).create()

            retrofit = Retrofit.Builder()
                    .client(client)
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
        }

        return retrofit
    }
}