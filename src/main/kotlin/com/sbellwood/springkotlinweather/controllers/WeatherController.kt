package com.sbellwood.springkotlinweather.controllers

import com.sbellwood.springkotlinweather.repositories.OpenWeatherApiRepository
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class WeatherController {

    private val openWeatherApiRepository = OpenWeatherApiRepository()

    @GetMapping("/weather")
    fun getWeather(model: Model): String {

        val forecastResponse = openWeatherApiRepository.fetchFiveDayForecast()

        model.addAttribute("forecast", forecastResponse)
        return "weather"
    }
}