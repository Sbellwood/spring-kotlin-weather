package com.sbellwood.springkotlinweather.enum

enum class MessageType {
    EMAIL, TEXT, PHONE_CALL
}