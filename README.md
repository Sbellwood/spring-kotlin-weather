# Spring-Kotlin-Weather
## Author: Skyler Bellwood
This is an exploratory project written in Kotlin using the Spring Framework. The project uses Retrofit to make an HTTP request to a weather api service and then displays the results in a very basic HTML page.

## How to Run Project
If your IDE of choice has Spring support, simply clone the repository and open the project in your IDE. Navigate to the `SpringKotlinWeatherApplication.kt` file and run the main method in the manner that your IDE of choice requires.

### Running in the command line
If your IDE does not support launching a Spring application, you can run Spring from the command line. Navigate to the root of the project in the Terminal or a similar application, then run the following command: `./gradlew bootRun`

Once the project is running, open a browser and navigate to `http://localhost:8080/weather` to see the output of the program.
